# author: jaskirat_singh1208

import pprint
import json
import time

import requests
from src import constants as const
import csv


class QCTicketCreator:

    def __init__(self, config, logger, env_type):
        self.config = config
        self.logger = logger
        self.data = const.credentials[env_type]
        self.login_url = const.login_url[env_type]
        self.pace_ticket_url = const.pace_ticket_url[env_type]

    def _get_access_token(self):
        """
        Get user credentials and produce the auth_token
        :return: nothing, just stores the variable auth_token in class
        """
        access_token = ''
        response = requests.post(self.login_url, data=self.data)
        res = json.loads(response.text)
        if res['response']:
            access_token = 'Bearer ' + res['response']['access_token']
        else:
            pprint.pprint(res.json())
            print("Failed to get access token, probably because of invalid credentials")
            exit(0)
        return access_token

    @staticmethod
    def generate_request_string_list(inputs):
        reqs = []
        for input_str in inputs:
            req = dict(zip(const.ESSENTIAL_PARAMS_LIST, input_str))
            print(str(req))
            reqs.append(json.dumps(req, indent=4))
        return reqs

    @staticmethod
    def _get_json_req_str_generator_from_file(path_to_file):
        """
        Reads the file: csv/xml. For every input line
        :return:
        """
        csv_file = open(path_to_file, "r")
        csv_reader = csv.reader(csv_file, delimiter=',')
        for row in csv_reader:
            request_str = dict(zip(const.ESSENTIAL_PARAMS_LIST, row))
            yield json.dumps(request_str)

    def raise_qc_tickets(self, path_to_file):
        access_token = self._get_access_token()
        request_str_generator = self._get_json_req_str_generator_from_file(path_to_file)
        headers = {
            const.AUTHORISATION_KEY: access_token,
            const.CONTENT_TYPE_KEY: const.APPLICATION_JSON_CONTENT_TYPE,
            'Accept': const.APPLICATION_JSON_CONTENT_TYPE
        }
        for request_str in request_str_generator:
            pprint.pprint(request_str)
            resp = requests.post(url=self.pace_ticket_url, data=request_str, headers=headers)
            pprint.pprint(resp.json())


if __name__ == '__main__':
    qc_ticket_creator = QCTicketCreator(1, 2, const.Environment.DEV)
    qc_ticket_creator.raise_qc_tickets('../temp.csv')

from enum import Enum

ENTITY_ID = 'entityId'
ENTITY_TYPE = 'entityType'
TYPE_ID = 'typeId'
ASSIGNEE_ID = 'assigneeId'
ASSIGNEE_TYPE = 'assigneeType'
OWNER_ID = 'ownerId'
OWNER_TYPE = 'ownerType'
SOURCE = 'source'
TITLE = 'title'
SUBJECT = 'subject'
AUTHORISATION_KEY = 'Authorization'
CONTENT_TYPE_KEY = 'Content-Type'

ESSENTIAL_PARAMS_LIST = [ENTITY_ID, ENTITY_TYPE, TYPE_ID, ASSIGNEE_ID, ASSIGNEE_TYPE,
                         OWNER_ID, OWNER_TYPE, SOURCE, TITLE, SUBJECT]

APPLICATION_JSON_CONTENT_TYPE = 'application/json'


class Environment(Enum):
    DEV = 0
    STAGING = 1
    PROD = 2


credentials = {
    Environment.DEV: {
        "username": ['DEV_USERNAME'],
        "password": ['DEV_PASSWORD'],
        "grant_type": ["password"],
        "client_id": ["sso"],
        "session_reset": ["yes"]
    },
    Environment.STAGING: {
        "username": ['STAGING_USERNAME'],
        "password": ['STAGING_PASSWORD'],
        "grant_type": ["password"],
        "client_id": ["sso"],
        "session_reset": ["yes"]
    },
    Environment.PROD: {
        "username": ['PROD_USERNAME'],
        "password": ['PROD_PASSWORD'],
        "grant_type": ["password"],
        "client_id": ["sso"],
        "session_reset": ["yes"]
    }
}
login_url = {
    Environment.DEV: 'https://login.stg.rivigo.com/sso/token',
    Environment.STAGING: 'https://login.stg.rivigo.com/sso/token',
    Environment.PROD: 'https://login.rivigo.com/sso/token'
}
pace_ticket_url = {
    Environment.DEV: 'https://zoom-ticketing-dev.stg.rivigo.com/ticket',
    Environment.STAGING: 'https://zoom-ticketing-dev.stg.rivigo.com/ticket',
    Environment.PROD: 'prod_url'
}
